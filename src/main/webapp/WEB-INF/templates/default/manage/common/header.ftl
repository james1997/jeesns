<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown logo-element-bm">
                    JEESNS
                </div>
            </li>

            <li>
                <a href="javascript:void(0)"><i class="fa fa-th-large"></i> <span class="nav-label">系统设置</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="active"><a href="${managePath}/config/edit" target="jeesns_iframe">系统设置</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><i class="fa fa-th-large"></i> <span class="nav-label">文章</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="${managePath}/cms/articleCate/list" target="jeesns_iframe">栏目管理</a></li>
                    <li><a href="${managePath}/cms/index" target="jeesns_iframe">文章管理</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><i class="fa fa-th-large"></i> <span class="nav-label">会员</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="${managePath}/mem/index" target="jeesns_iframe">会员管理</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><i class="fa fa-th-large"></i> <span class="nav-label">微博</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="${managePath}/weibo/index" target="jeesns_iframe">微博管理</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><i class="fa fa-th-large"></i> <span class="nav-label">群组</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="${managePath}/group/index" target="jeesns_iframe">群组管理</a></li>
                </ul>
            </li>
        </ul>

    </div>
</nav>

<div id="jeesns-wrapper" class="gray-bg">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top main-bg" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0)"><i class="fa fa-bars"></i> </a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="${base}/">
                        首页
                    </a>
                </li>
                <li>
                    <a href="${base}/member/logout">
                        <i class="fa fa-sign-out"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>