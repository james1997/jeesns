package com.lxinet.jeesns.core.utils;

/**
 * Created by zchuanzhao on 2017/1/5.
 */
public class ConfigUtil {
    public final static String CMS_POST = "cms_post";
    public final static String CMS_POST_REVIEW = "cms_post_review";
    public final static String GROUP_ALIAS = "group_alias";
    public final static String GROUP_APPLY = "group_apply";
    public final static String GROUP_APPLY_REVIEW = "group_apply_review";
    public final static String MEMBER_EMAIL_VALID = "member_email_valid";
    public final static String MEMBER_LOGIN_OPEN = "member_login_open";
    public final static String MEMBER_REGISTER_OPEN = "member_register_open";
    public final static String SITE_DESCRIPTION = "site_description";
    public final static String SITE_DOMAIN = "site_domain";
    public final static String SITE_KEYS = "site_keys";
    public final static String SITE_LOGO = "site_logo";
    public final static String SITE_NAME = "site_name";
    public final static String SITE_SEND_EMAIL_ACCOUNT = "site_send_email_account";
    public final static String SITE_SEND_EMAIL_PASSWORD = "site_send_email_password";
    public final static String SITE_SEND_EMAIL_SMTP = "site_send_email_smtp";
    public final static String SITE_SEO_TITLE = "site_seo_title";
    public final static String WEIBO_ALIAS = "weibo_alias";
    public final static String WEIBO_POST = "weibo_post";
    public final static String WEIBO_POST_MAXCONTENT = "weibo_post_maxcontent";

}
